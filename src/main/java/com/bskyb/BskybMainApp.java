package com.bskyb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;

/**
 * 
 * @author Petr Mikolas
 *
 */
@SpringBootApplication
public class BskybMainApp {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(BskybMainApp.class, args);
	}

}
