package com.bskyb.vaadin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Title("BSKYB Demo")
@Theme("sky")
@SpringUI
public class BskybVaadin extends UI {

	private static final Logger log = LoggerFactory.getLogger(BskybVaadin.class);

	@Override
	protected void init(VaadinRequest request) {

		log.info("--- call VAADIN UI");

		VerticalLayout root = new VerticalLayout();

		Label text = new Label("BSKYB Demo application");
		Button button = new Button(VaadinIcons.DESKTOP);

		button.addClickListener(e -> Notification.show("SKY frontend not implemented ...", Type.ERROR_MESSAGE));

		root.addComponents(text, button);

		root.setComponentAlignment(text, Alignment.TOP_CENTER);
		root.setComponentAlignment(button, Alignment.TOP_CENTER);

		setContent(root);

	}
}
