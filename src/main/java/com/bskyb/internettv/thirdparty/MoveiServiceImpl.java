package com.bskyb.internettv.thirdparty;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bskyb.enums.PCLevel;

/**
 * 
 * @author Petr Mikolas
 *
 */
@Service
public class MoveiServiceImpl implements MovieService {

	private static final Logger log = LoggerFactory.getLogger(MoveiServiceImpl.class);

	public String getParentalControlLevel(String titleId) throws TitleNotFoundException, TechnicalFailureException {

		log.info("--- call getParentalControlLevel()");

		if (titleId == null) {
			throw new TitleNotFoundException();
		}

//		String level = PCLevel.U.toString();
//		String level = PCLevel.PG.toString();
		String level = PCLevel.AGE_12.toString();
//		String level = PCLevel.AGE_15.toString();
//		String level = PCLevel.AGE_18.toString();
		return level;
	}
}
