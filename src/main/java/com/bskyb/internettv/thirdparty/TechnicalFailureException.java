package com.bskyb.internettv.thirdparty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author Petr Mikolas
 *
 */
@SuppressWarnings("serial")
public class TechnicalFailureException extends Exception {

	private static final Logger log = LoggerFactory.getLogger(TechnicalFailureException.class);

	@Override
	public void printStackTrace() {
		// super.printStackTrace();
		log.error("Exception: System error");
	}

}
