package com.bskyb.internettv.thirdparty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Petr Mikolas
 *
 */
@SuppressWarnings("serial")
public class TitleNotFoundException extends Exception {

	private static final Logger log = LoggerFactory.getLogger(TitleNotFoundException.class);

	@Override
	public void printStackTrace() {
		// super.printStackTrace();
		log.error("Exception: The movie service could not find the given movie");
	}

}
