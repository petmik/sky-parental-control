package com.bskyb.internettv.parental_control_service;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bskyb.enums.PCLevel;
import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;
import com.bskyb.internettv.thirdparty.TitleNotFoundException;

/**
 * 
 * @author Petr Mikolas
 *
 */
@Service
public class ParentalControlServiceImpl implements ParentalControlService {

	private static final Logger log = LoggerFactory.getLogger(ParentalControlServiceImpl.class);

	@Autowired
	MovieService movieService;

	public boolean canWatchMovie(String customerParentalControlLevel, String movieId) throws Exception {

		log.info("--- call canWatchMovie()");

		boolean canWatch = false;

		try {

			String movieLevel = movieService.getParentalControlLevel(movieId);
			PCLevel pcLevel = PCLevel.valueOf(customerParentalControlLevel);

			if (pcLevel.compareTo(PCLevel.valueOf(movieLevel)) <= 0) {
				canWatch = true;
				log.info("Customer can watch this movie");
			} else {
				canWatch = false;
				log.info("Customer cannot watch this movie, cause age restriction!");
			}

		} catch (TechnicalFailureException e) {
			// e.printStackTrace();
			log.error("System error");

		} catch (TitleNotFoundException e) {
			// e.printStackTrace();
			log.error("The movie service could not find the given movie");
		}

		return canWatch;

	}
}
