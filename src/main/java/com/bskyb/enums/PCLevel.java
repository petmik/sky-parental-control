package com.bskyb.enums;

public enum PCLevel {

	U, PG, AGE_12, AGE_15, AGE_18;

	private String level;

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
