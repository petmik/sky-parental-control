package com.bskyb.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bskyb.enums.PCLevel;
import com.bskyb.internettv.parental_control_service.ParentalControlService;

/**
 * @author Petr Mikolas
 * 
 *         This REST class represents Client (fake frontend)
 */
@RestController
@RequestMapping
public class ParentalRestController {

	private static final Logger log = LoggerFactory.getLogger(ParentalRestController.class);

	private ParentalControlService service;

	@Autowired
	public void setService(ParentalControlService service) {
		this.service = service;
	}

	@GetMapping("/sky")
	public boolean callParentalControlService() {

		log.info("--- call REST callParentalControlService()");
		boolean isWatch = false;

		try {

			// comment(uncomment) line to test scenarios of parental level, no movieID

			isWatch = service.canWatchMovie(PCLevel.U.toString(), "Some Movie");
//			isWatch = service.canWatchMovie(PCLevel.PG.toString(), "Some Movie");
//			isWatch = service.canWatchMovie(PCLevel.AGE_12.toString(), "Some Movie");
//			isWatch = service.canWatchMovie(PCLevel.AGE_15.toString(), "Some Movie");
//			isWatch = service.canWatchMovie(PCLevel.AGE_18.toString(), "Some Movie");

//			isWatch = service.canWatchMovie(PCLevel.AGE_15.toString(), null);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return isWatch;

	}

}
